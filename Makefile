# Makefile for libobjcsockets
# Compiles the code for the library
# Created on 8/26/2018
# Created by Andrew Davis
#
# Copyright (C) 2018  Andrew Davis
#
# Licensed under the Lesser GNU General Public License, version 3

# define the compiler
CC=gcc

# define the compiler flags for the library
CFLAGS=`gnustep-config --objc-flags` -x objective-c -fPIC -c -Wall
CFLAGS += -Wno-unused-but-set-variable -Wno-unused-result

# define the compiler flags for the test client
client: CFLAGS=`gnustep-config --objc-flags` -x objective-c -c -Wall -g

# define the compiler flags for the test server
server: CFLAGS=`gnustep-config --objc-flags` -x objective-c -c -Wall -g

# define linker flags for the library
LDFLAGS=-shared -lgnustep-base -lobjc 

# define linker flags for the test programs
TFLAGS=-lobjctermio -lgnustep-base -lobjc -L./lib -Wl,-rpath,./lib 
TFLAGS += -lobjcsockets

# retrieve source code for the library
LEXCE=$(shell ls src/lib/except/*.m)
LSOCK=$(shell ls src/lib/socket/*.m)
LADDR=$(shell ls src/lib/addr/*.m)

# list the source code for the library
LSOURCES=$(LEXCE) $(LSOCK) $(LADDR)

# compile the source code for the library
LOBJECTS=$(LSOURCES:.m=.o)

# retrieve source code for the test client
TCMAIN=$(shell ls src/test/client/*.m)

# list the source code for the test client
TCSOURCES=$(TCMAIN)

# compile the source code for the test client
TCOBJECTS=$(TCSOURCES:.m=.o)

# retrieve source code for the test server
TSMAIN=$(shell ls src/test/server/*.m)

# list the source code for the test server
TSSOURCES=$(TSMAIN)

# compile the source code for the test server
TSOBJECTS=$(TSSOURCES:.m=.o)

# define the name of the library
LIB=libobjcsockets.so

# define the name of the test client
TESTC=demo_client

# define the name of the test server
TESTS=demo_server

# rule for building both the library and the test programs
all: library client server 

# master rule for compiling the library
library: $(LSOURCES) $(LIB)

# master rule for compiling the test client
client: $(TCSOURCES) $(TESTC)

# master rule for compiling the test server
server: $(TSSOURCES) $(TESTS)

# sub-rule for compiling the library
$(LIB): $(LOBJECTS)
	$(CC) $(LOBJECTS) -o $@ $(LDFLAGS)
	mkdir lib
	mkdir lobj
	mv -f $(LOBJECTS) lobj/
	mv -f $@ lib/
	find . -type f -name '*.d' -delete

# sub-rule for compiling the test client
$(TESTC): $(TCOBJECTS)
	$(CC) $(TCOBJECTS) -o $@ $(TFLAGS)
	mkdir cbin
	mkdir cobj
	mv -f $(TCOBJECTS) cobj/
	mv -f $@ cbin/
	find . -type f -name '*.d' -delete

# sub-rule for compiling the test server
$(TESTS): $(TSOBJECTS)
	$(CC) $(TSOBJECTS) -o $@ $(TFLAGS)
	mkdir sbin
	mkdir sobj
	mv -f $(TSOBJECTS) sobj/
	mv -f $@ sbin/ 
	find . -type f -name '*.d' -delete 

# rule for compiling source code to object code
.m.o:
	$(CC) $(CFLAGS) $< -o $@

# target to install the compiled library
# REQUIRES ROOT
install:
	if [ -f "/usr/lib/libobjcsockets.so" ]; then \
		rm /usr/lib/libobjcsockets.so; \
	fi 
	if [ -d "/usr/include/objcsockets" ]; then \
		rm -rf /usr/include/objcsockets; \
	fi 
	
	mkdir /usr/include/objcsockets
	mkdir /usr/include/objcsockets/except
	mkdir /usr/include/objcsockets/socket 
	mkdir /usr/include/objcsockets/addr 
	cp src/lib/objcsockets.h /usr/include/objcsockets/
	cp $(shell ls src/lib/except/*.h) /usr/include/objcsockets/except/
	cp $(shell ls src/lib/socket/*.h) /usr/include/objcsockets/socket/
	cp $(shell ls src/lib/addr/*.h) /usr/include/objcsockets/addr/
	cp ./lib/$(LIB) /usr/lib/

# target to clean the workspace
clean:
	if [ -d "lobj" ]; then \
		rm -rf lobj; \
	fi
	if [ -d "lib" ]; then \
		rm -rf lib; \
	fi
	if [ -d "cbin" ]; then \
		rm -rf cbin; \
	fi
	if [ -d "cobj" ]; then \
		rm -rf cobj; \
	fi
	if [ -d "sbin" ]; then \
		rm -rf sbin; \
	fi
	if [ -d "sobj" ]; then \
		rm -rf sobj; \
	fi 

# end of Makefile
