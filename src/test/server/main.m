/*
 * main.m
 * Main code file for the libobjcsockets demo server
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "../../lib/objcsockets.h"
#import <objctermio/objctermio.h>

//main function - main entry point for the program
int main(int argc, const char* argv[]) {
	//create an autorelease pool
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];

	//create a ServerSocket instance
	ServerSocket* server = [[ServerSocket alloc] 
				initWithDomain: DOM_INET
				       andType: S_STREAM];

	//create a SockAddr instance to bind the server
	SockAddr* portData = [[SockAddr alloc] 
				initWithDomain: DOM_INET
					andPort: 8888];

	//attempt to bind the socket to a port
	@try {
		[server bindToPort: portData];
	} @catch(BindException* ex) {
		//handle the exception
		[Terminal writeString: [ex reason]];
		[Terminal writeChar: '\n'];

		//deallocate the objects
		[portData release];
		[server release];

		//and exit with an error
		return EXIT_FAILURE;
	}

	//print that the bind succeeded
	[Terminal writeString: @"Bind done!\n"];

	//make the server listen
	[server listenWithBacklogSize: 3];

	//print that the server is waiting
	[Terminal writeString: @"Waiting for incoming connections...\n"];

	//attempt to accept a connection
	@try {
		[server accept];
	} @catch(AcceptException* ex) {
		//handle the exception
		[Terminal writeString: [ex reason]];
		[Terminal writeChar: '\n'];

		//release the objects
		[portData release];
		[server release];

		//and exit with an error
		return EXIT_FAILURE;
	}

	//print that a connection was accepted
	[Terminal writeString: @"Connection accepted!\n"];

	//write a message to the client
	NSString* message = @"Hello, Client!\n";
	[server sendData: [message dataUsingEncoding: 
				NSUTF8StringEncoding]];

	//release the port data
	[portData release];

	//release the socket
	[server release];

	//drain the pool
	[pool drain];

	//and exit with no errors
	return EXIT_SUCCESS;
}

//end of program
