/*
 * main.m
 * Main code file for the libobjcsockets demo client
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "../../lib/objcsockets.h"
#import <objctermio/objctermio.h>

//main function - main entry point for the program
int main(int argc, const char* argv[]) {
	//create an autorelease pool
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];

	//attempt to create a socket
	Socket* socket = nil;
	@try {
		socket = [[Socket alloc] initWithDomain: DOM_INET
				andType: S_STREAM];
	} @catch(SocketException* ex) {
		//handle the exception
		[Terminal writeString: [ex reason]];
		[Terminal writeChar: '\n'];
		return EXIT_FAILURE;
	}

	//display a prompt
	[Terminal writeString: @"Enter the URL of a website: "];

	//read in a URL
	NSString* url = [Terminal readString];

	//attempt to create a SockAddr object to connect the socket to
	SockAddr* remote = nil;
	@try {
		//create a SockAddr object
		remote = [[SockAddr alloc] 
				initWithHostname: url
				       andDomain: DOM_INET
					 andPort: 80];
	} @catch(HostException* ex) {
		//handle the exception
		[Terminal writeString: [ex reason]];
		[Terminal writeChar: '\n'];

		//release the socket
		[socket release];

		//and exit with an error
		return EXIT_FAILURE;
	}

	//attempt to connect the socket to the remote address
	@try {
		[socket connectTo: remote];
	} @catch(ConnectException* ex) {
		//handle the exception
		[Terminal writeString: [ex reason]];
		[Terminal writeChar: '\n'];

		//release the socket
		[socket release];

		//release the address
		[remote release];

		//and exit with an error
		return EXIT_FAILURE;
	}

	//display that the socket connected
	[Terminal writeString: @"Connected!\n"];

	//attempt to send data to the server
	@try {
		//send a message to the server
		NSString* msg = @"GET / HTTP/1.1\r\n\r\n";
		[socket sendData: [msg dataUsingEncoding:
					NSUTF8StringEncoding]];
	} @catch(SendException* ex) {
		//handle the exception
		[Terminal writeString: [ex reason]];
		[Terminal writeChar: '\n'];

		//release the socket
		[socket release];

		//release the address
		[remote release];

		//and exit with an error
		return EXIT_FAILURE;
	}

	//write that the data sent successfully
	[Terminal writeString: @"Data sent successfully!\n"];

	//attempt to receive a reply from the server
	@try {
		//read data from the server
		NSData* readData = [socket receiveDataWithCount: 2000];

		//convert the data to a string
		NSString* readStr = [[NSString alloc] 
				initWithData: readData
				    encoding: NSUTF8StringEncoding];

		//print that the data was received successfully
		[Terminal writeString: @"Reply received!\n"];

		//print out the received data
		[Terminal writeString: readStr];
		[Terminal writeChar: '\n'];

		//and release the string
		[readStr release];

	} @catch(ReceiveException* ex) {
		//handle the exception
		[Terminal writeString: [ex reason]];
		[Terminal writeChar: '\n'];

		//release the socket
		[socket release];

		//release the address
		[remote release];

		//and exit with an error
		return EXIT_FAILURE;
	}

	//release the address
	[remote release];

	//and release the socket
	if(socket != nil) { //if the socket was created successfully
		//then release it
		[socket release];
	}

	//drain the pool
	[pool drain];

	//and exit with no errors
	return EXIT_SUCCESS;
}

//end of program
