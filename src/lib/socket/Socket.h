/*
 * Socket.h
 * Declares a class that represents a network socket
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "Domain.h"
#import "SockType.h"
#import "../addr/SockAddr.h"

//class declaration
@interface Socket : NSObject {
	//fields
	int _fd; //the file descriptor for the Socket
	Domain _domain; //the Socket's domain
	SockType _type; //the Socket's type
	bool _isOpen; //is the Socket open?
	bool _connected; //is the Socket connected?
	SockAddr* _currentConnection; //the current connection data
}

//property declarations
@property (readonly) Domain domain;
@property (readonly) SockType type;
@property (readonly) bool isOpen;
@property (readonly) bool connected;
@property (readonly) SockAddr* currentConnection;

//method declarations

//initializes a Socket instance
- (id) initWithDomain: (Domain) newDomain andType: (SockType) newType;

//deallocates a Socket instance
- (void) dealloc;

//copies a Socket instance
- (id) copy;

//closes a Socket's connection
- (void) close;

//connects a Socket to a remote address
- (void) connectTo: (SockAddr*) address;

//sends data to a remote address
- (void) sendData: (NSData*) data;

//receives data from a remote address
- (NSData*) receiveDataWithCount: (int) byteCount;

@end //end of header
