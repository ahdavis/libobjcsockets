/*
 * ServerSocket.m
 * Implements a class that represents a server socket
 * Created on 8/29/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the class header
#import "ServerSocket.h"

//import the BindException header
#import "../except/BindException.h"

//import the SockAddr+Data category header
#import "../addr/SockAddr+Data.h"

//import the SockAddr+Init category header
#import "../addr/SockAddr+Init.h"

//import the AcceptException header
#import "../except/AcceptException.h"

//import the sockaddr_in header
#import <netinet/in.h>

//import the socket header
#import <sys/socket.h>

//import the inet header
#import <arpa/inet.h>

//import the POSIX header
#import <unistd.h>

//class implementation
@implementation ServerSocket

//property synthesis
@synthesize bindData = _bindData;
@synthesize isBound = _isBound;
@synthesize clientConnection = _clientConnection;
@synthesize hasClient = _hasClient;

//init method - initializes a ServerSocket instance
- (id) initWithDomain: (Domain) newDomain andType: (SockType) newType {
	//call the superclass init method
	self = [super initWithDomain: newDomain andType: newType];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_bindData = nil;
		_isBound = NO;
		_clientConnection = nil;
		_hasClient = NO;
		_clientFD = -1;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates a ServerSocket instance
- (void) dealloc {
	//release the bind data
	if(_bindData != nil) {
		[_bindData release];
	}

	//clear the bound flag
	_isBound = NO;

	//release the client data
	if(_clientConnection != nil) {
		[_clientConnection release];
	}

	//clear the client flag
	_hasClient = NO;

	//and call the superclass dealloc method
	[super dealloc];
}

//copy method - copies a ServerSocket instance
- (id) copy {
	//return a copy of the instance
	return [[ServerSocket alloc] initWithDomain: _domain
			andType: _type];
}

//overridden connectTo method - does nothing
- (void) connectTo: (SockAddr*) address {
	//do nothing
}

//binds the ServerSocket to a port
- (void) bindToPort: (SockAddr*) portData {
	//attempt to bind the socket to the port
	int res = bind(_fd, (struct sockaddr*)[portData dataPtr],
			sizeof([portData data]));

	//verify that the bind succeeded
	if(res < 0) { //if the bind failed
		//then throw an exception
		@throw [BindException exceptionWithPort: [portData port]];
	} else { //if the bind succeeded
		//then update the fields
		_bindData = portData;
		[_bindData retain];
		_isBound = YES;
	}
}

//listen method - makes the ServerSocket listen on its port
- (void) listenWithBacklogSize: (int) backlogSize {
	//listen on the port
	listen(_fd, backlogSize);
}

//accept method - makes the ServerSocket accept an incoming connection
- (void) accept {
	//make sure that the ServerSocket is bound to a port
	if(_isBound) { //if the ServerSocket is bound to a port
		//then accept a connection
		
		//get the size of a sockaddr_in structure
		int addrSize = sizeof(struct sockaddr_in);

		//declare a sockaddr_in structure to hold the
		//client data
		struct sockaddr_in data;

		//zero out the client data
		memset(&data, 0, sizeof(struct sockaddr_in));

		//attempt to accept the connection
		_clientFD = accept(_fd, 
				(struct sockaddr*)&data,
				(socklen_t*)&addrSize);

		//make sure that the accept succeeded
		if(_clientFD < 0) { //if the accept failed
			//then throw an exception
			@throw [AcceptException 
				exceptionWithPort: [_bindData port]];
		} else { //if the accept succeeded
			//then init the client connection field
			_clientConnection = 
				[[SockAddr alloc] initWithStruct: data];

			//and set the client flag
			_hasClient = YES;
		}
	}
}

//overridden sendData method - sends data to the client
- (void) sendData: (NSData*) data {
	//make sure that the server is connected to the client
	if(_hasClient) { //if a client is connected
		//then send the data
		write(_clientFD, [data bytes], [data length]);
	}
}

//overridden receiveDataWithCount method - receives data from the client
- (NSData*) receiveDataWithCount: (int) byteCount {
	//declare the return value
	NSData* ret = nil;

	//make sure that the client is connected
	if(_hasClient) { //if the client is connected
		//then receive the data
		
		//declare a buffer to hold the incoming data
		unsigned char buf[byteCount];

		//read into the buffer
		read(_clientFD, buf, byteCount);

		//and create an NSData object from the buffer
		ret = [NSData dataWithBytes: buf length: byteCount];
	}

	//and return the data
	return ret;
}

@end //end of implementation
