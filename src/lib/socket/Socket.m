/*
 * Socket.m
 * Implements a class that represents a network socket
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//include the POSIX header
#include <unistd.h>

//include the sockaddr_in header
#include <netinet/in.h>

//import the class header
#import "Socket.h"

//include the socket header
#include <sys/socket.h>

//import the SocketException header
#import "../except/SocketException.h"

//import the ConnectException header
#import "../except/ConnectException.h"

//import the SendException header
#import "../except/SendException.h"

//import the SockAddr+Data category header
#import "../addr/SockAddr+Data.h"

//import the ReceiveException header
#import "../except/ReceiveException.h"

//class implementation
@implementation Socket

//property synthesis
@synthesize domain = _domain;
@synthesize type = _type;
@synthesize isOpen = _isOpen;
@synthesize connected = _connected;
@synthesize currentConnection = _currentConnection;

//init method - initializes a Socket instance
- (id) initWithDomain: (Domain) newDomain andType: (SockType) newType {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_domain = newDomain;
		_type = newType;
		_isOpen = NO;
		_connected = NO;
		_currentConnection = nil;

		//initialize the socket's file descriptor
		_fd = socket(_domain, _type, 0);

		//and make sure that the socket creation succeeded
		if(_fd == -1) { //if the creation failed
			//then throw an exception
			@throw [SocketException 
			exceptionWithDomain: _domain 
				andSockType: _type];
		} else { //if the creation succeeded
			_isOpen = YES; //then set the open flag
		}
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates a Socket instance
- (void) dealloc {
	[self close]; //close the Socket
	[super dealloc]; //and call the superclass dealloc method
}

//copy method - copies a Socket instance
- (id) copy {
	//return a copy of the Socket
	return [[Socket alloc] initWithDomain: _domain andType: _type];
}

//close method - closes a Socket's connection
- (void) close {
	//release the current connection
	if(_currentConnection != nil) {
		[_currentConnection release];
	}

	//close the Socket
	close(_fd);

	//and clear the flags
	_isOpen = NO;
	_connected = NO;
}

//connectTo method - connects to a remote address
- (void) connectTo: (SockAddr*) address {
	//attempt to connect to the remote address
	int res = connect(_fd, (struct sockaddr*)[address dataPtr],
				sizeof([address data]));

	//and verify that the connection succeeded
	if(res < 0) { //if the connection failed
		//then throw an exception
		@throw [ConnectException exceptionWithIP: [address address]
						 andPort: [address port]];
	} else { //if the connection succeeded
		//then set the connected flag
		_connected = YES;

		//and set the current connection field
		_currentConnection = address;
		[_currentConnection retain];
	}
}

//sendData method - sends data to a remote server
- (void) sendData: (NSData*) data {
	//make sure that the Socket is connected
	if(_connected) { //if the socket is connected
		//then attempt to send the message
		int res = send(_fd, [data bytes], 
				[data length], 0);

		//and make sure that the send succeeded
		if(res < 0) { //if the send failed
			//then throw an exception
			@throw [SendException  
				exceptionWithCount: [data length]
				andAddress: _currentConnection];
		}
	}
}

//receiveData method - receives data from a remote server
- (NSData*) receiveDataWithCount: (int) byteCount {
	//make sure that the Socket is connected
	if(_connected) { //if the socket is connected
		//then declare a buffer to read into
		unsigned char buf[byteCount];

		//attempt to read into the buffer
		int res = recv(_fd, buf, byteCount, 0);

		//make sure that the receive succeeded
		if(res < 0) { //if the receive failed
			//then throw an exception
			@throw 
		[ReceiveException exceptionWithCount: byteCount
			andAddress: _currentConnection];
		}

		//and return the received bytes
		return [NSData dataWithBytes: buf length: byteCount];
	} else { //if the Socket is not connected
		//then return an empty block of data
		return [NSData data];
	}
}

@end //end of implementation
