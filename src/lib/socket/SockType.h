/*
 * SockType.h
 * Enumerates types of sockets
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//include
#include <sys/socket.h>

//enum definition
typedef enum {
	S_STREAM = SOCK_STREAM, //TCP socket
	S_DGRAM = SOCK_DGRAM, //UDP socket
	S_SEQPACKET = SOCK_SEQPACKET, //sequenced packet socket
	S_RAW = SOCK_RAW, //raw network protocol access
	S_RDM = SOCK_RDM //reliable UDP socket
} SockType;

//end of header
