/*
 * Domain.h
 * Enumerates socket address families
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//include
#include <sys/socket.h>

//enum definition
typedef enum {
	DOM_UNIX = AF_UNIX, //local UNIX communication
	DOM_LOCAL = AF_LOCAL, //generic local communication
	DOM_INET = AF_INET, //IPv4 communication
	DOM_INET6 = AF_INET6, //IPv6 communication
	DOM_IPX = AF_IPX, //IPX/Novell communication
	DOM_NETLINK = AF_NETLINK, //kernel communication
	DOM_X25 = AF_X25, //radio communication
	DOM_AX25 = AF_AX25, //amateur radio communication
	DOM_ATMPVC = AF_ATMPVC, //ATM communication
	DOM_APPLETALK = AF_APPLETALK, //AppleTalk communication
	DOM_PACKET = AF_PACKET, //low-level packet interface
	DOM_ALG = AF_ALG //kernel crypto interface
} Domain;

//end of header
