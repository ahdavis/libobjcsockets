/*
 * ServerSocket.h
 * Declares a class that represents a server socket
 * Created on 8/29/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "Socket.h"
#import "Domain.h"
#import "SockType.h"
#import "../addr/SockAddr.h"

//class declaration
@interface ServerSocket : Socket {
	//fields
	SockAddr* _bindData; //the data for a bound ServerSocket
	bool _isBound; //is the ServerSocket bound to a port?
	int _clientFD; //the file descriptor for a connected client
	SockAddr* _clientConnection; //the data for a connected client
	bool _hasClient; //does the ServerSocket have a client?
}

//property declarations
@property (readonly) SockAddr* bindData;
@property (readonly) bool isBound;
@property (readonly) SockAddr* clientConnection;
@property (readonly) bool hasClient;

//method declarations

//initializes a ServerSocket instance
- (id) initWithDomain: (Domain) newDomain andType: (SockType) newType;

//deallocates a ServerSocket instance
- (void) dealloc;

//copies a ServerSocket instance
- (id) copy;

//connects a ServerSocket to a remote server (DOES NOTHING)
- (void) connectTo: (SockAddr*) address;

//binds a ServerSocket to a port
- (void) bindToPort: (SockAddr*) portData;

//makes a ServerSocket listen
- (void) listenWithBacklogSize: (int) backlogSize;

//makes a ServerSocket accept an incoming connection
- (void) accept;

//sends data to the client
- (void) sendData: (NSData*) data;

//receives data from the client
- (NSData*) receiveDataWithCount: (int) byteCount;

@end //end of header
