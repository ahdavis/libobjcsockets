/*
 * SockAddr+Data.m
 * Implements a category that adds data getters to the SockAddr class
 * Created on 8/27/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the category header
#import "SockAddr+Data.h"

//category implementation
@implementation SockAddr (Data)

//dataPtr method - returns a pointer to the data field
- (struct sockaddr_in*) dataPtr {
	return &_data; //return the address of the data field
}

//data method - returns the data field by value
- (struct sockaddr_in) data {
	return _data; //return the data field
}

@end //end of implementation
