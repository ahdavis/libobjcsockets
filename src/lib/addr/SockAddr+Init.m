/*
 * SockAddr+Init.m
 * Implements a category that adds the ability to initialize a SockAddr
 * instance with a sockaddr_in structure instance
 * Created on 8/29/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the category header
#import "SockAddr+Init.h"

//import the socket header
#import <sys/socket.h>

//import the inet header
#import <arpa/inet.h>

//category implementation
@implementation SockAddr (Init)

//init method - initializes a SockAddr instance
- (id) initWithStruct: (struct sockaddr_in) data {
	//call the IP init method
	return [self initWithIP: 
		[[NSString alloc] initWithCString: 
			inet_ntoa(data.sin_addr)]
		andDomain: data.sin_family 
		  andPort: ntohs(data.sin_port)];
}

@end //end of implementation
