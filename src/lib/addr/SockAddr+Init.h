/*
 * SockAddr+Init.h
 * Declares a category that adds the ability to initialize a SockAddr
 * instance with a sockaddr_in structure instance
 * Created on 8/29/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import <netinet/in.h>
#import "SockAddr.h"

//category declaration
@interface SockAddr (Init)

//method declaration

//initializes a SockAddr instance
- (id) initWithStruct: (struct sockaddr_in) data;

@end //end of header
