/*
 * SockAddr.h
 * Declares a class that represents a network socket address
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#include <netinet/in.h>
#import <Foundation/Foundation.h>
#import "../socket/Domain.h"

//class declaration
@interface SockAddr : NSObject {
	//fields
	struct sockaddr_in _data; //the address data
	NSString* _address; //the IP address string
	Domain _domain; //the address family
	int _port; //the address port
}

//property declarations
@property (readonly) NSString* address;
@property (readonly) Domain domain;
@property (readonly) int port;

//method declarations

//initializes a SockAddr instance with a hostname
- (id) initWithHostname: (NSString*) name andDomain: (Domain) newDomain
	andPort: (int) newPort;

//initializes a SockAddr instance with an IP
- (id) initWithIP: (NSString*) ip andDomain: (Domain) newDomain
	andPort: (int) newPort;

//initializes a SockAddr instance with no specific IP
- (id) initWithDomain: (Domain) newDomain andPort: (int) newPort;

//deallocates a SockAddr instance
- (void) dealloc;

//copies a SockAddr instance
- (id) copy;

@end //end of header
