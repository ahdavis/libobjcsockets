/*
 * SockAddr.m
 * Implements a class that represents a network socket address
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the class header
#import "SockAddr.h"

//import the socket headers
#import <sys/socket.h>
#import <netdb.h>
#import <arpa/inet.h>

//import the HostException header
#import "../except/HostException.h"

//private method declarations
@interface SockAddr ()

//generates an IP string from a hostname string
- (NSString*) ipFromHostname: (NSString*) name;

@end //end of private methods

//class implementation
@implementation SockAddr

//property synthesis
@synthesize address = _address;
@synthesize domain = _domain;
@synthesize port = _port;

//first init method - initializes a SockAddr instance from a hostname
- (id) initWithHostname: (NSString*) name andDomain: (Domain) newDomain
	andPort: (int) newPort {
	//call the second init method
	return [self initWithIP: [self ipFromHostname: name]
		      andDomain: newDomain andPort: newPort];
}

//second init method - initializes a SockAddr instance from an IP
- (id) initWithIP: (NSString*) ip andDomain: (Domain) newDomain
	andPort: (int) newPort {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		memset(&_data, 0, sizeof(_data)); //zero the data field
		_address = ip; //init the address field
		[_address retain]; //and retain it
		_domain = newDomain; //init the domain field
		_port = newPort; //init the port field

		//and init the data field
		_data.sin_addr.s_addr = inet_addr([_address UTF8String]);
		_data.sin_family = _domain;
		_data.sin_port = htons(_port);
	}

	//and return the instance
	return self;
}

//third init method - initializes a SockAddr instance with no specific IP
- (id) initWithDomain: (Domain) newDomain andPort: (int) newPort {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		memset(&_data, 0, sizeof(_data));
		_address = [[NSString alloc] initWithFormat: @"%i",
				INADDR_ANY];
		_domain = newDomain;
		_port = newPort;

		//and init the data field
		_data.sin_addr.s_addr = INADDR_ANY;
		_data.sin_family = _domain;
		_data.sin_port = htons(_port);
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates a SockAddr instance
- (void) dealloc {
	memset(&_data, 0, sizeof(_data)); //zero the data field
	[_address release]; //release the address field
	[super dealloc]; //and call the superclass dealloc method
}

//copy method - copies a SockAddr instance
- (id) copy {
	//return a copy of the instance
	return [[SockAddr alloc] initWithIP: _address
			andDomain: _domain andPort: _port];
}

//private ipFromHostname method - generates an IP string
//from a hostname string
- (NSString*) ipFromHostname: (NSString*) name {
	//get the database entry for the host
	struct hostent* he = NULL;
	he = gethostbyname([name UTF8String]);

	//make sure that the retrieval succeeded
	if(he == NULL) { //if the retrieval failed
		//then throw an exception
		@throw [HostException exceptionWithHostname: name];
	}

	//get the address list from the hostname
	struct in_addr** addr_list = (struct in_addr**)he->h_addr_list;

	//loop and assemble the IP string from the address list
	NSString* ret = [[NSString alloc] initWithString: @""];
	int i;
	for(i = 0; addr_list[i] != NULL; i++) {
		//append each address to the IP string
		ret = [ret stringByAppendingString: 
			[NSString stringWithFormat: @"%s",
			inet_ntoa(*addr_list[i])]];
	}

	//and return the IP string
	return ret;
}

@end //end of implementation
