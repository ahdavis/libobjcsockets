/*
 * SockAddr+Data.h
 * Declares a category that adds data getters to the SockAddr class
 * Created on 8/27/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import <netinet/in.h>
#import "SockAddr.h"

//category declaration
@interface SockAddr (Data)

//method declarations

//returns a pointer to the data field
- (struct sockaddr_in*) dataPtr;

//returns the data field by value
- (struct sockaddr_in) data;

@end //end of header
