/*
 * SendException.h
 * Declares an exception that is thrown when
 * a data sending operation fails
 * Created on 8/27/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "../addr/SockAddr.h"

//class declaration
@interface SendException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes a SendException instance
- (id) initWithCount: (int) count andAddress: (SockAddr*) address;

//deallocates a SendException instance
- (void) dealloc;

//generates an autoreleased SendException instance
+ (SendException*) exceptionWithCount: (int) count 
	andAddress: (SockAddr*) address;

@end //end of header
