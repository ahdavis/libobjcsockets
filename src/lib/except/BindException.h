/*
 * BindException.h
 * Declares an exception that is thrown when a server fails to
 * bind to a port
 * Created on 8/28/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface BindException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes a BindException instance
- (id) initWithPort: (int) badPort;

//deallocates a BindException instance
- (void) dealloc;

//generates an autoreleased BindException instance
+ (BindException*) exceptionWithPort: (int) badPort;

@end //end of header
