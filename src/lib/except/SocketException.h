/*
 * SocketException.h
 * Declares an exception that is thrown when a socket fails to create
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "../socket/Domain.h"
#import "../socket/SockType.h"

//class declaration
@interface SocketException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes a SocketException instance
- (id) initWithDomain: (Domain) badDomain 
	andSockType: (SockType) badSockType;

//deallocates a SocketException instance
- (void) dealloc;

//returns an autoreleased SocketException instance
+ (SocketException*) exceptionWithDomain: (Domain) badDomain
	andSockType: (SockType) badSockType;

@end //end of header
