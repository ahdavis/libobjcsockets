/*
 * SocketException.m
 * Implements an exception that is thrown when a socket fails to create
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the class header
#import "SocketException.h"

//class implementation
@implementation SocketException

//init method - initializes a SocketException instance
- (id) initWithDomain: (Domain) badDomain
	andSockType: (SockType) badSockType {
	//call the superclass init method
	self = [super initWithName: @"SocketException"
		reason: [NSString 
stringWithFormat: @"Failed to initialize socket! Domain: %i Type: %i",
		badDomain, badSockType]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates a SocketException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - returns an autoreleased SocketException
//instance
+ (SocketException*) exceptionWithDomain: (Domain) badDomain
	andSockType: (SockType) badSockType {
	//generate an exception instance
	return [[[SocketException alloc] initWithDomain: badDomain
			andSockType: badSockType] autorelease];
}

@end //end of implementation
