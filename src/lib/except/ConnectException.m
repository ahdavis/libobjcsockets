/*
 * ConnectException.m
 * Implements an exception that is thrown when a socket connection fails
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the class header
#import "ConnectException.h"

//class implementation
@implementation ConnectException

//init method - initializes a ConnectException instance
- (id) initWithIP: (NSString*) ip andPort: (int) port {
	//call the superclass init method
	self = [super initWithName: @"ConnectException"
reason: [NSString stringWithFormat: @"Failed to connect to %@ on port %i!",
		ip, port] 
			userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates a ConnectException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased ConnectException
//instance
+ (ConnectException*) exceptionWithIP: (NSString*) ip andPort: (int) port {
	//return a ConnectException instance
	return [[[ConnectException alloc] initWithIP: ip andPort: port]
		autorelease];
}

@end //end of implementation
