/*
 * AcceptException.m
 * Implements an exception that is thrown when a server socket
 * fails to accept an incoming connection
 * Created on 8/29/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the class header
#import "AcceptException.h"

//class implementation
@implementation AcceptException

//init method - initializes an AcceptException instance
- (id) initWithPort: (int) badPort {
	//call the superclass init method
	self = [super initWithName: @"AcceptException"
reason: [NSString 
	stringWithFormat: @"Failed to accept connection on port %i.",
		badPort]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an AcceptException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased 
//AcceptException instance
+ (AcceptException*) exceptionWithPort: (int) badPort {
	//return an autoreleased AcceptException instance
	return [[[AcceptException alloc] initWithPort: badPort]
			autorelease];
}

@end //end of implementation
