/*
 * ConnectException.h
 * Declares an exception that is thrown when a socket connection fails
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface ConnectException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes a ConnectException instance
- (id) initWithIP: (NSString*) ip andPort: (int) port;

//deallocates a ConnectException instance
- (void) dealloc;

//generates an autoreleased ConnectException instance
+ (ConnectException*) exceptionWithIP: (NSString*) ip andPort: (int) port;

@end //end of header
