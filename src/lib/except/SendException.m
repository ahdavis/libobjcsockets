/*
 * SendException.m
 * Implements an exception that is thrown when
 * a data sending operation fails
 * Created on 8/27/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the class header
#import "SendException.h"

//class implementation
@implementation SendException

//init method - initializes a SendException instance
- (id) initWithCount: (int) count andAddress: (SockAddr*) address {
	//call the superclass init method
	self = [super initWithName: @"SendException"
reason: [NSString 
stringWithFormat: @"Failed to send %i bytes to %@ on port %i.", count,
	[address address], [address port]]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates a SendException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased SendException instance
+ (SendException*) exceptionWithCount: (int) count
		andAddress: (SockAddr*) address {
	//generate a SendException instance
	return [[[SendException alloc] initWithCount: count
			andAddress: address] autorelease];
}

@end //end of implementation
