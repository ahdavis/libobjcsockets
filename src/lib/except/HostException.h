/*
 * HostException.h
 * Declares an exception that is thrown when a hostname fails
 * to resolve
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface HostException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes a HostException instance
- (id) initWithHostname: (NSString*) badHostname;

//deallocates a HostException instance
- (void) dealloc;

//generates an autoreleased HostException instance
+ (HostException*) exceptionWithHostname: (NSString*) badHostname;

@end //end of header
