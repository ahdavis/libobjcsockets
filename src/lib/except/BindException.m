/*
 * BindException.m
 * Implements an exception that is thrown when a server fails to
 * bind to a port
 * Created on 8/28/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the class header
#import "BindException.h"

//class implementation
@implementation BindException

//init method - initializes a BindException instance
- (id) initWithPort: (int) badPort {
	//call the superclass init method
	self = [super initWithName: @"BindException"
reason: [NSString stringWithFormat: @"Failed to bind to port %i.",
		badPort]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates a BindException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased BindException instance
+ (BindException*) exceptionWithPort: (int) badPort {
	//return an autoreleased BindException instance
	return [[[BindException alloc] initWithPort: badPort]
		autorelease];
}

@end //end of implementation
