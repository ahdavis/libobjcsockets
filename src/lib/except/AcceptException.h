/*
 * AcceptException.h
 * Declares an exception that is thrown when a server socket
 * fails to accept an incoming connection
 * Created on 8/29/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface AcceptException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an AcceptException instance
- (id) initWithPort: (int) badPort;

//deallocates an AcceptException instance
- (void) dealloc;

//generates an autoreleased AcceptException instance
+ (AcceptException*) exceptionWithPort: (int) badPort;

@end //end of header
