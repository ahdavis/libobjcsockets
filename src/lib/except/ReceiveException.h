/*
 * ReceiveException.h
 * Declares an exception that is thrown when a socket fails to receive data
 * Created on 8/27/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "../addr/SockAddr.h"

//class declaration
@interface ReceiveException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes a ReceiveException instance
- (id) initWithCount: (int) count andAddress: (SockAddr*) address;

//deallocates a ReceiveException instance
- (void) dealloc;

//generates an autoreleased ReceiveException instance
+ (ReceiveException*) exceptionWithCount: (int) count 
	andAddress: (SockAddr*) address;

@end //end of header
