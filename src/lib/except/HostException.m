/*
 * HostException.m
 * Implements an exception that is thrown when a hostname fails
 * to resolve
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the class header
#import "HostException.h"

//class implementation
@implementation HostException

//init method - initializes a HostException instance
- (id) initWithHostname: (NSString*) badHostname {
	//call the superclass init method
	self = [super initWithName: @"HostException"
	reason: [NSString stringWithFormat: @"Failed to resolve %@.",
			badHostname]
				userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates a HostException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased HostException instance
+ (HostException*) exceptionWithHostname: (NSString*) badHostname {
	//generate a HostException instance
	return [[[HostException alloc] initWithHostname: badHostname]
			autorelease];
}

@end //end of implementation
