/*
 * ReceiveException.m
 * Implements an exception that is thrown when 
 * a socket fails to receive data
 * Created on 8/27/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the class header
#import "ReceiveException.h"

//class implementation
@implementation ReceiveException

//init method - initializes a ReceiveException instance
- (id) initWithCount: (int) count andAddress: (SockAddr*) address {
	//call the superclass init method
	self = [super initWithName: @"ReceiveException"
reason: [NSString 
stringWithFormat: @"Failed to receive %i bytes from %@ on port %i.",
	count, [address address], [address port]]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates a ReceiveException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased 
//ReceiveException instance
+ (ReceiveException*) exceptionWithCount: (int) count 
	andAddress: (SockAddr*) address {
	//generate a ReceiveException instance
	return [[[ReceiveException alloc] initWithCount: count
			andAddress: address] autorelease];
}

@end //end of implementation
